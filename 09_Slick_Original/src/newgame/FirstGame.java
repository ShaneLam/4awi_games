package newgame;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.tests.AnimationTest;

public class FirstGame extends BasicGame {
	private double x_rec;
	private double y_rec;
	private double x_cir;
	private double y_cir;
	private double y_ova;
	private int direction_rec = 0;
	private int direction_cir = 0;
	private int direction_ova = 0;

	public FirstGame() {
		super("FirstGame");
	}
	
	
	@Override
	public void render(GameContainer gamecontainer, Graphics graphics) throws SlickException {
		graphics.drawRect((int)this.x_rec,(int)this.y_rec , 200, 100);
		graphics.drawOval((int)this.x_cir,300, 100, 100);
		graphics.drawOval(200,(int)this.y_ova, 100, 150);

	}

	@Override
	public void init(GameContainer gamecontainer) throws SlickException {
		this.x_rec = 100;
	}

	@Override
	public void update(GameContainer gamecontainer, int delta) throws SlickException {
		//this.x_rec += (double)delta*0.5;
		
		//Rechteck
		if(direction_rec == 0) {
			this.x_rec += .3 * delta;
			if(x_rec > 580)
			{
				direction_rec = 1;
			}
			
			
		}
		
		if(direction_rec == 1) {
			this.y_rec += .3 * delta;
			if(y_rec > 480)
			{
				direction_rec = 2;
			}
			
		}
		
		if(direction_rec == 2) {
			this.x_rec -= .3 * delta;
			if(x_rec < 5)
			{
				direction_rec = 3;
			}
			
		}
		
		if(direction_rec == 3) {
			this.y_rec -= .3 * delta;
			if(y_rec < 5)
			{
				direction_rec = 0;
			}
			
		}
		
		
		//Kreis
		if(direction_cir == 0) {
			this.x_cir += .3 * delta;
			if(x_cir > 580)
			{
				direction_cir = 1;
			}
			
			
		}
		
		if(direction_cir == 1) {
			this.x_cir -= .3 * delta;
			if(x_cir < 5)
			{
				direction_cir = 0;
			}
			
			
		}
		
		//Oval
		this.y_ova += .3 * delta;
		if(this.y_ova > 800)
		{
			this.y_ova = -300;
		}
	
		

	}
	
	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new FirstGame());
			container.setDisplayMode(800,600,false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

}
