package at.sla.games.bulletfight.actors;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;



public class Player implements Actors {
	
	private float x,y;
	@SuppressWarnings("unused")
	private boolean isMoving;
	private boolean goRight;
	private boolean goLeft;
	private Bullet2 bullet2;
	
	
	public Player(float x, float y) {
		super();
		this.x = x;
		this.y = y;
	}


	@Override
	public void render(Graphics graphics) {
		
		graphics.drawRect((int) this.x, (int) this.y,50,50);

	}
	
	
	public float getX() {
		return x;
	}


	public void setX(float x) {
		this.x = x;
	}


	public float getY() {
		return y;
	}


	public void setY(float y) {
		this.y = y;
	}


	public void isMovingRight()
	{
		this.goRight = true;
		
	}
	public void isnotMovingRight()
	{
		this.goRight = false;
		
	}
	public void isMovingLeft()
	{
		this.goLeft = true;
		
	}
	public void isnotMovingLeft()
	{
		this.goLeft = false;
	}

	public void goRight() {
		this.goRight = true;
		
	}

	public void goLeft() {
		this.goRight = false;
		

	}

	public void Stop() {
		this.isMoving = false;

	}


	@Override
	public void update(int delta, GameContainer container) {
		if (goRight == true) {
			x++;
			
		} 
		if (goLeft == true) {
			x--;
			
		} 

	}
	
	public boolean collide(Bullet2 bullet2)
	{
		return false;
	}

}
