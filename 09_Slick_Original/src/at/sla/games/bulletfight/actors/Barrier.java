package at.sla.games.bulletfight.actors;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

import at.SL.FirstGameOOP.Actors;

public class Barrier implements Actors, at.sla.games.bulletfight.actors.Actors {
	private double x,y;
	private int width, height;
	private int direction = 0;

	public Barrier(double x, double y, int width, int height) {
		super();
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	@Override
	public void render(Graphics graphics) {
		graphics.drawRect((int)this.x, (int)this.y, width, height);

	}

	@Override
	public void update(int delta, GameContainer container) {
		if(direction == 0) {
			this.x += .3 * delta;
			if(x > 580)
			{
				direction = 1;
			}
			
			
		}
		
		if(direction == 1) {
			this.x -= .3 * delta;
			if(x < 5)
			{
				direction = 0;
			}
			
			
		}

	}

}
