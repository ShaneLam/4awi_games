package at.sla.games.bulletfight.actors;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

import at.SL.FirstGameOOP.Actors;

public class Bullet implements Actors, at.sla.games.bulletfight.actors.Actors {
	private double x,y;

	public Bullet(double x, double y) {
		super();
		this.x = x;
		this.y = y;
	}

	@Override
	public void render(Graphics graphics) {
		graphics.drawRect((int) this.x,(int) this.y , 20, 20);

	}

	@Override
	public void update(int delta, GameContainer container) {
		 this.y--;
		 
	}
	
	public double getX() {
		return x;
	}


	public void setX(double x) {
		this.x = x;
	}


	public double getY() {
		return y;
	}


	public void setY(double y) {
		this.y = y;
	}

}
