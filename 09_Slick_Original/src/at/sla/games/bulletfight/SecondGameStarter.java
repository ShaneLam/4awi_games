package at.sla.games.bulletfight;

import java.util.ArrayList;
import java.util.List;


import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

import at.sla.games.bulletfight.actors.Actors;
import at.sla.games.bulletfight.actors.Barrier;
import at.sla.games.bulletfight.actors.Bullet;
import at.sla.games.bulletfight.actors.Bullet2;
import at.sla.games.bulletfight.actors.Player;
import newgame.FirstGame;
@SuppressWarnings("unused")

public class SecondGameStarter extends BasicGame{
	private List<Actors> actors;
	private Player player1;
	private Player player2;
	private Bullet2 bullet2;


	public SecondGameStarter() {
		super("FirstGame");
		
	}

	@Override
	public void render(GameContainer container, Graphics graphics) throws SlickException {
		for (Actors actor : actors) {

			actor.render(graphics);
		}
		
	}

	@Override
	public void init(GameContainer container) throws SlickException {
		this.actors = new ArrayList<>();
		this.player1 = new Player(500,540);
		this.player2 = new Player(500,10);
		this.bullet2 = new Bullet2(this.player2.getX(),this.player2.getY());
		this.actors.add(new Barrier(100,280,200,20));
		this.actors.add(player1);
		this.actors.add(player2);
		this.actors.add(bullet2);
		
		
		
	}

	@Override
	public void update(GameContainer container, int delta) throws SlickException {
		
		for (Actors actor : actors) {

			actor.update(delta, container);
		}
		
		if(this.player1.collide(bullet2))
		{
			container.getGraphics().setColor(Color.red);
		}else
		{
			container.getGraphics().setColor(Color.white);
		}
		
	}
	
	@Override
	public void keyPressed(int key, char c) {
		// TODO Auto-generated method stub
		super.keyPressed(key, c);
		if(key == Input.KEY_LEFT)
		{
			this.player1.isMovingLeft();
		}
		else if(key == Input.KEY_RIGHT)
		{
			this.player1.isMovingRight();
		}
		else if(key == Input.KEY_A)
		{
			this.player2.isMovingLeft();
		}
		else if(key == Input.KEY_D)
		{
			this.player2.isMovingRight();
		}
		else if(key == Input.KEY_UP)
		{
			this.actors.add(new Bullet(this.player1.getX(),this.player1.getY()));
			
		}
		else if(key == Input.KEY_W)
		{
			this.actors.add(new Bullet2(this.player2.getX(),this.player2.getY()));
			
		}
		
	}
	
	@Override
	public void keyReleased(int key, char c) {
		// TODO Auto-generated method stub
		super.keyReleased(key, c);
		if(key == Input.KEY_LEFT)
		{
			this.player1.isnotMovingLeft();
		}
		else if(key == Input.KEY_RIGHT)
		{
			this.player1.isnotMovingRight();
		}
		else if(key == Input.KEY_A)
		{
			this.player2.isnotMovingLeft();
		} 
		else if(key == Input.KEY_D)
		{
			this.player2.isnotMovingRight();
		} 
		
	
		
	}
	
	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new SecondGameStarter());
			container.setDisplayMode(800,600,false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}
}
