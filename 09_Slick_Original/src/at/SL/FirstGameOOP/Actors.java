package at.SL.FirstGameOOP;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public interface Actors {
	
	public void render(Graphics graphics);
	void update(int delta, GameContainer container);

}