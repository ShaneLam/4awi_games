package at.SL.FirstGameOOP;

import java.util.Random;

import org.newdawn.slick.Graphics;

public class Snowflake {
	private double x, y, speed;
	private int size;

	public Snowflake(int size, double speed) {
		super();
		this.speed = speed;
		setRandPosition();

		if (size == 0) {
			this.size = 5;
		} else if (size == 1) {
			this.size = 8;
		} else {
			this.size = 10;
		}

	}

	private void setRandPosition() {
		Random r = new Random();
		this.y = r.nextInt(600) - 600;
		this.x = r.nextInt(800);
	}

	public void update(int delta) {
		this.y += (double) delta * this.speed;
		if (this.y > 800) {
			setRandPosition();
		}

	}

	public void render(Graphics graphics) {
		graphics.fillOval((int) x, (int) y, size, size);
		// graphics.drawOval((int)this.x, (int)this.y, size, size);
	}

}
