package at.SL.FirstGameOOP;



import java.util.List;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;

public class StarShip implements Actors {
	private double x,y;
	private boolean goRight = false;
	private boolean goLeft = false;
	private boolean shoot = false;
	private List<Actors> actors;
	private Projectile projectile;
	
	
	public StarShip(double x, double y) {
		super();
		this.x = x;
		this.y = y;
	}

	@Override
	public void update(int delta, GameContainer container) {
		goLeft = container.getInput().isKeyDown(Input.KEY_LEFT);
		goRight = container.getInput().isKeyDown(Input.KEY_RIGHT);
		shoot = container.getInput().isKeyDown(Input.KEY_SPACE);
		
		
		if(goRight == true) {
			x++;
		}if(goLeft == true) {
			x--;
		}if(shoot == true) {
			this.actors.add(new Projectile((int)this.x,(int)this.y));
		}
		
	}

	@Override
	public void render(Graphics graphics) {
		graphics.drawRect((int)this.x, (int)this.y,100,100);
		graphics.fillRect((int)this.x, (int)this.y,100,100);	
	}
	
	
}
