package at.SL.FirstGameOOP;
import org.newdawn.slick.Graphics;

public class HTL_Rectangle {
	
	private double x,y;
	private int width, height;
	private int direction = 0;
	
	public HTL_Rectangle(double x, double y, int width, int height) {
		super();
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}
	
	public void update(int delta) {
		if(direction == 0) {
			this.x += .3 * delta;
			if(x > 580)
			{
				direction = 1;
			}
			
			
		}
		
		if(direction == 1) {
			this.y += .3 * delta;
			if(y > 480)
			{
				direction = 2;
			}
			
		}
		
		if(direction == 2) {
			this.x -= .3 * delta;
			if(x < 5)
			{
				direction = 3;
			}
			
		}
		
		if(direction == 3) {
			this.y -= .3 * delta;
			if(y < 5)
			{
				direction = 0;
			}
			
		}
		
	}
	
	public void render(Graphics graphics) {
		graphics.drawRect((int)this.x, (int)this.y, width, height);
	}

}
