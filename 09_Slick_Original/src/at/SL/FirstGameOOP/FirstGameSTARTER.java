package at.SL.FirstGameOOP;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;


public class FirstGameSTARTER extends BasicGame {
	private HTL_Rectangle rectangle;
	private HTL_Circle circle;
	private HTL_Oval oval;
	private List<Snowflake> snowflakes;
	private List<Actors>actors;
	private StarShip StarShip;
	private Projectile Projectile;
	private ShootingStar shootingstar;
	
	public FirstGameSTARTER() {	
		super("FirstGame");	
	}public void addSnowflake(Snowflake snowflake)
	{
		this.snowflakes.add(snowflake);
	}
	
	
	@Override
	public void render(GameContainer gamecontainer, Graphics graphics) throws SlickException {
		this.rectangle.render(graphics);
		this.circle.render(graphics);
		this.oval.render(graphics);	
		this.StarShip.render(graphics);
		//this.shootingstar.render(graphics);
		for(Snowflake snowflake : snowflakes) {
			snowflake.render(graphics);
		
		}								

	}
			
	@Override
	public void init(GameContainer gamecontainer) throws SlickException {
		this.actors = new ArrayList<>();
		this.rectangle = new HTL_Rectangle(10,100,150,150);
		this.circle = new HTL_Circle(10,300,100,100);
		this.oval = new HTL_Oval(100,10,100,100);
		this.snowflakes = new ArrayList<>();
		this.StarShip = new StarShip(300,500);
		this.actors.add(StarShip);
		//this.actors.add(shootingstar);
		//this.actors.add(Projectile);
		this.shootingstar = new ShootingStar(0.5,20);
		for(int i = 0; i<50; i++) {
			this.snowflakes.add(new Snowflake(0,0.3));
		}
		
		for(int i = 0; i<50; i++) {
			this.snowflakes.add(new Snowflake(1,0.4));
		}
		
		for(int i = 0; i<50; i++) {
			this.snowflakes.add(new Snowflake(2,0.5));
		}
	
	}

	
	@Override
	public void update(GameContainer gamecontainer, int delta) throws SlickException {
		this.rectangle.update(delta);
		this.circle.update(delta);
		this.oval.update(delta);
		this.StarShip.update(delta, gamecontainer);
		//this.shootingstar.update(delta, gamecontainer);
		for(Snowflake snowflake : snowflakes) {
			snowflake.update(delta);
		}
		
	}
	
	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new FirstGameSTARTER());
			container.setDisplayMode(800,600,false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

}
