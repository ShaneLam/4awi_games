package at.SL.FirstGameOOP;

import org.newdawn.slick.Graphics;

public class HTL_Circle {
	private double x,y;
	private int width, height;
	private int direction = 0;
	
	
	public HTL_Circle(double x, double y, int width, int height) {
		super();
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}
	
	public void update(int delta) {
		if(direction == 0) {
			this.x += .3 * delta;
			if(x > 580)
			{
				direction = 1;
			}
			
			
		}
		
		if(direction == 1) {
			this.x -= .3 * delta;
			if(x < 5)
			{
				direction = 0;
			}
			
			
		}
	}
	
	public void render(Graphics graphics) {
		graphics.drawOval((int)this.x, (int)this.y, width, height);
	}
	

}
