package at.SL.FirstGameOOP;

import org.newdawn.slick.Graphics;

public class HTL_Oval {
	
	private double x,y;
	private int width, height;
	
	public HTL_Oval(double x, double y, int width, int height) {
		super();
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}
	
	public void update(int delta) {
		this.y += .3 * delta;
		if(this.y > 800)
		{
			this.y = -300;
		}
	
	}
	
	public void render(Graphics graphics) {
		graphics.drawOval((int)this.x, (int)this.y, width, height);
	}
	
	

}
